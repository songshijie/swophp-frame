# swophp-frame

#### Description
Build a PHP framework based on swoole from scratch. At present, it supports HTTP and websocket protocols.
The purpose of this project is to learn about the swoole extension and the underlying principles of the framework.

#### Installation

```
composer create-project top-songshijie/swophp-frame
```

#### Instructions

- Http server

```bash
[root@swophp swophp-frame]# php ./bin/swophp http:start
```

- WebSocket server

```bash
[root@swophp swophp-frame]# php ./bin/swophp ws:start
```
## License

Swoft is an open-source software licensed under the [LICENSE](LICENSE)
