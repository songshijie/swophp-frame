# swophp-frame

#### 介绍
从零开始打造的基于swoole的php框架。目前支持http、websocket协议。
此项目目的是学习swoole扩展，以及框架的底层原理。

#### 安装教程

```
composer create-project top-songshijie/swophp-frame
```

#### 使用说明

- Http server

```bash
[root@swophp swophp-frame]# php ./bin/swophp http:start
```

- WebSocket server

```bash
[root@swophp swophp-frame]# php ./bin/swophp ws:start
```

## License

Swoft is an open-source software licensed under the [LICENSE](LICENSE)

