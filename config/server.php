<?php
/**
 * Author: top-songshijie
 * DateTime: 2020/03/22 16:47
 */

return [
    'http'=>[
        'host' => '0.0.0.0',
        'port' => 9501,
        'swoole' => [
            'task_worker_num' => 0
        ]
    ],
    'websocket'=>[
        'host' => '0.0.0.0',
        'port' => 9502,
        'swoole' => [
            'task_worker_num' => 0
        ]
    ]

];
