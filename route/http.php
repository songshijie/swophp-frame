<?php
/**
 * Author: top-songshijie
 * DateTime: 2020/03/21 13:31
 */

use SwoPhp\Route\Route;

Route::get('/index', 'IndexController@index');
Route::get('/test', 'IndexController@test');
