<?php
/**
 * Author: top-songshijie
 * DateTime: 2020/03/21 13:33
 */

namespace App\Http\Controller;

class IndexController
{
    public function index()
    {
        return 'this is index';
    }

    public function test()
    {
        return 'this is test';
    }

}
