<?php
/**
 * Author: top-songshijie
 * DateTime: 2020/03/25 15:06
 */

namespace App\WebSocket\Controller;

class IndexController
{
    public function open($server, $request)
    {
        dd('open');
        $server->push($server->fd, "open\n");
    }

    public function message($server, $frame)
    {
        dd('message');
        $server->push($frame->fd, "message\n");
    }

    public function onclose($server, $fd)
    {
        dd('close');
    }

}
